# Livestream from USB camera #

This README would normally document steps are necessary to get your usb camera stream to wowza.

### Prerequisite ###

* Ubuntu 16.04 
(Or Ubuntu Mate 16.04 for Raspberry Pi)
https://ubuntu-mate.org/raspberry-pi/
* ROS Kinetic
http://wiki.ros.org/kinetic/Installation/Ubuntu
* FFmpeg 
sudo apt-get install ffmpeg libavcodec-dev libavformat-dev libavutil-dev libswscale-dev libavresample-dev libavdevice-dev -y
* v4l-utils
sudo apt-get install v4l-utils

### How to set up ###

* Clone the source code
    - git clone https:/your_username@bitbucket.org/Mai_Duong_Hieu/usb_camera_stream.git
* Catkin make the workspace
    - cd usb_camera_stream/
    - ./install
* Configure your camera souce
    - nano ./src/usb_cam/launch/usb_cam.launch
    - Set width/height,.. for your camera. 640x480 is a recommended resolution.
* Configure stream node
    - nano ./src/stream/launch/stream.launch
    - Set the parameters for your account

### How to run ###

* First, run the usb_camera node
    - ./usb_cam
* Then, launch stream node
    - ./stream
* Check on wowza server
http://52.26.162.200:8088/enginemanager/Home.htm#application/_defaultVHost_/live/live/incomingstreams_details/&instance=_definst_&stream=myStream2&returnPage=incomingstreams

### Who do I talk to? ###

* If there were any problems, please contact Onboard Team.
or via email: hieu.md@workhouse.me