#ifndef STREAM_H
#define STREAM_H

#include <iostream>
#include <vector>

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <sensor_msgs/CameraInfo.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>

extern "C" {
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
}

enum STREAMER_STATUS {
    UNINITIALIZED = 0,
    INITIALIZING,       // getting camera info
    READY,
    IDLE,
    STREAMING
};

class CameraStreamer {
    public:
    CameraStreamer();
    ~CameraStreamer();
      
    void startStreaming();
    void stopStreaming();    

    private:

    // -------------------------attribute for streaming job---------------------------------------
    //private method for streamming
    bool initialize_streamer();
    void initialize_avformat_context(AVFormatContext *&fctx, const char *format_name);
    void initialize_io_context(AVFormatContext *&fctx, const char *output);
    void set_codec_params(AVFormatContext *&fctx, AVCodecContext *&codec_ctx, int width, int height, int fps, int kbit_rate);
    void initialize_codec_stream(AVStream *&stream, AVCodecContext *&codec_ctx, AVCodec *&codec);
    SwsContext *initialize_sample_scaler(AVCodecContext *codec_ctx, int width, int height);
    AVFrame *allocate_frame_buffer(AVCodecContext *codec_ctx, int width, int height);
    void write_frame(AVCodecContext *codec_ctx, AVFormatContext *fmt_ctx, AVFrame *frame);


    //stream attribute       
    AVFormatContext *ofmt_ctx;// = nullptr;
    AVCodec *out_codec;// = nullptr;
    AVStream *out_stream;// = nullptr;
    AVCodecContext *out_codec_ctx;// = nullptr;
    SwsContext *swsctx;
    AVFrame *frame;    

    // -------------------------streaming input---------------------------------------
    //stream configurations    
    int _frame_height;
    int _frame_width;
    int _fps;
    int _kbit_rate;
    std::string camera_info_topic;
    std::string camera_image_topic;
    std::string _stream_protocol;
    std::string _username;
    std::string _password;
    std::string _app_name;
    std::string _server_ip;
    std::string _streaming_id;
    std::string destination; 
    uint8_t _streamer_status;

    void set_stream_config(int width, int height, std::string image_topic, int fps = 20, int kbit_rate = 400);

    // -------------------------ros wrapper---------------------------------------
    //ros handle
    ros::NodeHandle _nh;
    //image transport
    image_transport::ImageTransport _it;   

    //! camera image subscribers
    image_transport::Subscriber camera_image_sub;

    //! canera info subscribers
    ros::Subscriber camera_info_sub;

    //implement service start streaming and stop streaming
    ros::ServiceServer start_service, stop_service;

    bool initSubcriber();    

    //! callback function
    void cameraInfoCallback(const sensor_msgs::CameraInfo::ConstPtr& cam_info);
    void cameraImageCallback(const sensor_msgs::ImageConstPtr& img);
};

#endif