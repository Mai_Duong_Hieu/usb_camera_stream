#include "camera_streamer.h"

int main(int argc, char **argv)
{
    /* code */
    ros::init(argc, argv, "stream_main_node");
    ros::NodeHandle nh;
    ros::NodeHandle nh_private("~");

    ROS_INFO("stream_main_node is activated successfully");

    CameraStreamer myStream;
    
    ros::spin();

    myStream.stopStreaming();

    return 0;
}
