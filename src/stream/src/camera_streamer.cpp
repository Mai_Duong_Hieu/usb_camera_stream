#include "camera_streamer.h"
#include <unistd.h>

CameraStreamer::CameraStreamer(): _it(_nh) {
    //set status
    _streamer_status = STREAMER_STATUS::UNINITIALIZED;

    //get ros node parameters
    // nh.param("destination", destination, std::string("rtmp://camera001:camera001@52.26.162.200:1935/live/myStream2"));
    _nh.param("server_ip", _server_ip, std::string("52.26.162.200") );
    _nh.param("protocol", _stream_protocol, std::string("rtmp") );
    _nh.param("username", _username, std::string("camera001") );
    _nh.param("password", _password, std::string("camera001") );
    _nh.param("app_name", _app_name, std::string("myStream2") );
    destination = _stream_protocol + "://" + _username + ":" + _password + "@" + _server_ip + ":1935/live/" + _app_name;
    std::cout << "stream url: " << destination << std::endl;
    _nh.param("camera_info_topic", camera_info_topic, std::string("/usb_cam/camera_info") );
    _nh.param("camera_image_topic", camera_image_topic, std::string("/usb_cam/image_raw") );
    _nh.param("stream_fps", _fps, 20);     
    _nh.param("kbit_rate", _kbit_rate, 500);
    
    initSubcriber();
}

CameraStreamer::~CameraStreamer() {
    
}



void CameraStreamer::initialize_avformat_context(AVFormatContext *&fctx, const char *format_name)
{
    int ret = avformat_alloc_output_context2(&fctx, nullptr, format_name, nullptr);
    if (ret < 0)
    {
        std::cout << "Could not allocate output format context!" << std::endl;
        exit(1);
    }
}

void CameraStreamer::initialize_io_context(AVFormatContext *&fctx, const char *output)
{
    if (!(fctx->oformat->flags & AVFMT_NOFILE))
    {
        int ret = avio_open2(&fctx->pb, output, AVIO_FLAG_WRITE, nullptr, nullptr);
        if (ret < 0)
        {
        std::cout << "Could not open output IO context!" << std::endl;
        exit(1);
        }
    }
}

void CameraStreamer::set_codec_params(AVFormatContext *&fctx, AVCodecContext *&codec_ctx, int width, int height, int fps, int kbit_rate)
{
    const AVRational dst_fps = {fps, 1};

    codec_ctx->codec_tag = 0;
    codec_ctx->codec_id = AV_CODEC_ID_H264;
    codec_ctx->codec_type = AVMEDIA_TYPE_VIDEO;
    codec_ctx->width = width;
    codec_ctx->height = height;
    codec_ctx->gop_size = 12;
    codec_ctx->pix_fmt = AV_PIX_FMT_YUV420P;
    codec_ctx->framerate = dst_fps;
    codec_ctx->time_base = av_inv_q(dst_fps);
    codec_ctx->bit_rate = (kbit_rate*1000);
    if (fctx->oformat->flags & AVFMT_GLOBALHEADER)
    {
        codec_ctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
    }
}

void CameraStreamer::initialize_codec_stream(AVStream *&stream, AVCodecContext *&codec_ctx, AVCodec *&codec)
{
    int ret = avcodec_parameters_from_context(stream->codecpar, codec_ctx);
    if (ret < 0)
    {
    std::cout << "Could not initialize stream codec parameters!" << std::endl;
    exit(1);
    }

    AVDictionary *codec_options = nullptr;
    av_dict_set(&codec_options, "profile", "high444", 0);
    av_dict_set(&codec_options, "preset", "superfast", 0);
    av_dict_set(&codec_options, "tune", "zerolatency", 0);

    // open video encoder
    ret = avcodec_open2(codec_ctx, codec, &codec_options);
    if (ret < 0)
    {
    std::cout << "Could not open video encoder!" << std::endl;
    exit(1);
    }
}

SwsContext *CameraStreamer::initialize_sample_scaler(AVCodecContext *codec_ctx, int width, int height)
{
    SwsContext *swsctx = sws_getContext(width, height, AV_PIX_FMT_BGR24, width, height, codec_ctx->pix_fmt, SWS_BICUBIC, nullptr, nullptr, nullptr);
    if (!swsctx)
    {
        std::cout << "Could not initialize sample scaler!" << std::endl;
        exit(1);
    }

    return swsctx;
}

AVFrame *CameraStreamer::allocate_frame_buffer(AVCodecContext *codec_ctx, int width, int height)
    {
    AVFrame *frame = av_frame_alloc();

    std::vector<uint8_t> framebuf(av_image_get_buffer_size(codec_ctx->pix_fmt, width, height, 1));
    av_image_fill_arrays(frame->data, frame->linesize, framebuf.data(), codec_ctx->pix_fmt, width, height, 1);
    frame->width = width;
    frame->height = height;
    frame->format = static_cast<int>(codec_ctx->pix_fmt);

    return frame;
}

void CameraStreamer::write_frame(AVCodecContext *codec_ctx, AVFormatContext *fmt_ctx, AVFrame *frame) {
    AVPacket pkt = {0};
    av_init_packet(&pkt);
    // std::cout << "write_frame: av_init_packet done\n";

    int ret = avcodec_send_frame(codec_ctx, frame);
    // std::cout << ret << std::endl;
    if (ret < 0)
    {
        std::cout << "Error sending frame to codec context!" << std::endl;
        exit(1);
    }

    ret = avcodec_receive_packet(codec_ctx, &pkt);
    // std::cout << ret << std::endl;
    if (ret < 0)
    {
        std::cout << "Error receiving packet from codec context!" << std::endl;
        exit(1);
    }

    av_interleaved_write_frame(fmt_ctx, &pkt);
    // std::cout << "write_frame: av_interleaved_write_frame done\n";
    av_packet_unref(&pkt);
    // std::cout << "write_frame: av_packet_unref done\n";
}

bool CameraStreamer::initialize_streamer()
{
    ROS_INFO("Setting up streamer");
    av_register_all();
    avformat_network_init();

    const char *output = destination.c_str();
    std::cout << "out put url" <<output<<std::endl;

    int ret;

    initialize_avformat_context(ofmt_ctx,"flv");
    initialize_io_context(ofmt_ctx,output);

    out_codec = avcodec_find_encoder(AV_CODEC_ID_H264);
    out_stream = avformat_new_stream(ofmt_ctx, out_codec);
    out_codec_ctx = avcodec_alloc_context3(out_codec);

    set_codec_params(ofmt_ctx,out_codec_ctx,_frame_width,_frame_height,_fps,_kbit_rate);
    initialize_codec_stream(out_stream,out_codec_ctx, out_codec);

    out_stream->codecpar->extradata = out_codec_ctx->extradata;
    out_stream->codecpar->extradata_size = out_codec_ctx->extradata_size;

    av_dump_format(ofmt_ctx,0,output,1);

    swsctx = initialize_sample_scaler(out_codec_ctx, _frame_width, _frame_height);
    frame = allocate_frame_buffer(out_codec_ctx, _frame_width, _frame_height);

    ret = avformat_write_header(ofmt_ctx,nullptr);
    //avformat_init_output
    if(ret < 0) {
        std::cout << "Could not write header!" << std::endl;
        // exit(1);
        return false;
    }
    return true;
}


void CameraStreamer::startStreaming() {

    if(_streamer_status==STREAMER_STATUS::READY)
    {

        _streamer_status = STREAMER_STATUS::IDLE;
        ROS_INFO("started streamer");
    }

}

void CameraStreamer::stopStreaming() {

    if(_streamer_status==STREAMER_STATUS::STREAMING||_streamer_status==STREAMER_STATUS::IDLE) {
        _streamer_status = STREAMER_STATUS::READY;
        av_write_trailer(ofmt_ctx);
        av_frame_free(&frame);
        avcodec_close(out_codec_ctx);
        avio_close(ofmt_ctx->pb);
        avformat_free_context(ofmt_ctx);
    }    
}

void CameraStreamer::cameraImageCallback(const sensor_msgs::ImageConstPtr& msg) {

    if (_streamer_status==STREAMER_STATUS::IDLE)
    {
        _stream_protocol = STREAMER_STATUS::STREAMING;    
        static uint32_t count_frame = 0;

        std::cout << "Streaming frame " <<count_frame<< "..."<<std::endl;
        std::vector<uint8_t> imgbuf(_frame_height * _frame_width * 3 + 16);

        // convert ros image to opencv image
        cv_bridge::CvImagePtr cv_ptr;
        try
        {
            if (sensor_msgs::image_encodings::isColor(msg->encoding)) {
                // cv_ptr = cv_bridge::toCvShare(msg, sensor_msgs::image_encodings::BGR8);
                cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
            }
            else {
                // cv_ptr = cv_bridge::toCvShare(msg, sensor_msgs::image_encodings::MONO8);
                cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::MONO8);
            }
         
            // cv_ptr = cv_bridge::toCvShare(msg, sensor_msgs::image_encodings::BGR8);
        }
        catch (cv_bridge::Exception& e)
        {
            ROS_ERROR("cv_bridge exception: %s", e.what());
            // system("PAUSE"); 
        }

        cv::Mat image(_frame_height, _frame_width, CV_8UC3, imgbuf.data(), _frame_width * 3);
        
        image = cv_ptr->image;
                
        std::cout << "===>1.got frame." << std::endl;
        const int stride[] = {static_cast<int>(image.step[0])};
        std::cout << "===>2.cast frame."<< std::endl;
        sws_scale(swsctx, &image.data, stride, 0, image.rows, frame->data, frame->linesize);
        std::cout << "===>3.sws_scale."<< std::endl;
        frame->pts += av_rescale_q(1, out_codec_ctx->time_base, out_stream->time_base);
        count_frame++;
        std::cout << "===>4.av_scale."<< std::endl;
        write_frame(out_codec_ctx, ofmt_ctx, frame);
        std::cout << "===>5.wrote frame"<< std::endl;

        _stream_protocol = STREAMER_STATUS::IDLE;
        
    }
    
}

void CameraStreamer::cameraInfoCallback(const sensor_msgs::CameraInfo::ConstPtr& cam_info)
{
    if(_streamer_status==STREAMER_STATUS::UNINITIALIZED) {
        ROS_INFO("STREAMER_STATUS::INITIALIZING");
        ROS_INFO("camera info callback");
        ROS_INFO("camera_height:%d - camera_width:%d",cam_info->height, cam_info->width);
        _frame_height = cam_info->height;
        _frame_width = cam_info->width;
        _streamer_status = STREAMER_STATUS::INITIALIZING;
        if(initialize_streamer())
        {
            ROS_INFO("init streamer done.");
            _streamer_status = STREAMER_STATUS::READY;;
        }
        else
        {
            ROS_ERROR("init streamer error!");
        }
        startStreaming();

    }   
}

bool CameraStreamer::initSubcriber()
{
    camera_image_sub = _it.subscribe(camera_image_topic,1,&CameraStreamer::cameraImageCallback,this);
    camera_info_sub = _nh.subscribe<sensor_msgs::CameraInfo>(camera_info_topic,1,&CameraStreamer::cameraInfoCallback,this);
    return true;
}

void CameraStreamer::set_stream_config(int width, int height, std::string image_topic, int fps, int kbit_rate)
{
    _frame_height = height;
    _frame_width = width;
    _fps = fps;
    _kbit_rate = kbit_rate;
    // camera_image_topic = image_topic;
}